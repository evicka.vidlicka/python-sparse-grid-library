import numpy as np
from ..src.kpn_lev_table import kpn_lev_table
import warnings

def lev2knots_lin(i):
    """relation level / number of points:
        m = i
    
       [m] = lev2knots_lin(i)
       i: level in each direction
       m: number of points to be used in each direction
    """
    return np.array(i)

def lev2knots_doubling(i):
    """relation level / number of points:
        m = 2^{i-1}+1, for i>1
        m=1            for i=1
        m=0            for i=0
    
     i.e. m(i)=2*m(i-1)-1
    """
    i = np.array(i)
    m = 2 ** (i-1.)+1.
    m[np.where(i == 0)] = 0
    m[np.where(i == 1)] = 1
    m = m.astype(int)
    return m

def lev2knots_2step(i):
    """relation level / number of points: m = 2i-1, i.e. m = 1,3,5,7,...
    i: level on each direction
    m: number of points to be used in each direction.
    """
    i = np.array(i)
    m = 2*i-1
    m[np.where(i == 0)] = 0
    return m

def lev2knots_kpn(I):
    """returns the number of knots corresponding to the i-level i via the 
    i2l map tabulated as in kpn_lev_table.
    """
    # are there non tabulated levels?
    I = np.array(I)
    non_tab = I[np.where(I > 5)]
    if np.size(non_tab) == 0:
        # nb knots is a vector after this instruction; I is understood 
        # coloumnwise as I(:).
        # Note that I want to access rows I+1, because minimum value for level 
        # is 0, whose data are stored in row 1.
        vect_nb_knots = kpn_lev_table(I, 2).T
        return np.reshape(vect_nb_knots, np.shape(I))
    else:
        warnings.Warn('SparseGKit:KpnNonTab; asking for non tabulated levels')
        r, c = np.shape(I)
        pos = np.where(I>5)
        vect_nb_knots = kpn_lev_table(I+1, 3).T
        vect_nb_knots[pos] = np.inf
        return np.reshape(vect_nb_knots, (r,c))