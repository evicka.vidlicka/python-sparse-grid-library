import numpy as np

def get_interval_map(a, b, dtype):
    """defines functions to shift sparse grids. 
    
    map = get_interval_map(a,b,'uniform') where a,b are two vectors, returns 
    a function that takes column points from (-1,1)^N to the generic box
    
            ( a(1) b(1) ) x ( a(2) b(2) ) x ( a(3) b(3) ) ...
            
    map = get_interval_map(a,b,'gaussian') where a,b are two vectors, returns 
    a function that takes points from R^N chosen according to standard gaussian
    measure to points in R^N chosen according to a product of gaussians with 
    means in a and stdev in b.
    
    In both cases the output function must be used as
    
        X = interval_map(T)
        
    where T is a matrix of points in (-1,1)^N, one point per column (as in the 
    output of reduce_sparse_grid or tensor_grid). Similarly X is a matrix of 
    points in ( a(1) b(1) ) x ( a(2) b(2) ) x ( a(3) b(3) ), one point per column.
    """
    a = np.array(a)
    b = np.array(b)
    
    if dtype == 'uniform':
        # first we build D, resize matrix: it modifies a column vector "c" with
        # components in 0,1 into a column vector "d" with components in 
        # ( 0 |b(1)-a(1)| ) x ( 0 |b(2)-a(2)| ) x ...
        D = np.diag(b - a)
        
        # next a translation vector: shifts a column vector "d" with components
        # in ( 0 |b(1)-a(1)| ) x ( 0 |b(2)-a(2)| ) x ... to "e" with components
        # in ( a(1) b(1) ) x ( a(2) b(2) ) x ...
        
        return lambda T: 0.5*np.dot(D, (T + np.ones(np.shape(T)))) \
                    + np.dot(np.reshape(a, (-1,1)), np.ones((1, np.size(T,1))))
    elif dtype == 'gaussian':
        # D is the stdev matrix
        D = np.diag(b)
        # shift vector, according to means
        return lambda T: np.dot(D, T) + np.dot(np.reshape(a, (-1,1)), 
                                np.ones((1, np.size(T,1))))
    else:
        raise ValueError('SparseGKit: type of distribution not recognized.')
                        