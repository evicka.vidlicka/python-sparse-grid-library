import numpy as np

def lagr_eval(current_knot, other_knots, nngrid_pts):
    """L = lagr_eval(current_knot,other_knots,non_grid_points)
    builds the lagrange function L(x) s.t. L(current_knot)=1; L(other_knots)=0;
    and returns L=L(non_grid_points)
    """
    other_knots = np.array(other_knots)
    nngrid_pts = np.array(nngrid_pts)
    # each monodim lagrange function is a product of K terms like 
    # (x-x_k)/(current_knot-x_k), so I compute it with a for loop
    
    # this is the number of iterations
    K = np.size(other_knots)
    # L is the result. It is a column vector, same size as non_grid_points. 
    # It is initialized to 1, and then iteratively multiplied by each factor.
    L = np.ones(np.size(nngrid_pts))
    
    for i in range(K):
        # these are the non current-knot, one at a time
        knots_i = other_knots[i]
        # here it comes the computation of the lagrangian factor 
        # (x-x_k)/(current_knot-x_k)
        L *= np.divide((nngrid_pts-knots_i),(current_knot-knots_i))
    return L
    
def lagr_eval_multidim(current_knot, knots_per_dim, non_grid_points):
    """L = agr_eval_multidim(current_knot,knots_per_dim,non_grid_points)
    evaluates the multidim Lagrange function L centered in current_knot 
    in non_grid_points.
    
    -> non_grid_points is an array of arrays; each row is a point to be evaluated
    -> knots_per_dim is a list of arrays. 
    In the arrays are stored all the knots in each direction. E.g.
    knots_per_dim=[np.array([0 0.5 1]),
               np.array([0 0.2 0.4 0.6 0.8 1])]
    it has to contain current_knot.
    """
    non_grid_points = np.array(non_grid_points)
#    N = np.size(non_grid_points, 1)
#    try:
#        P = np.size(non_grid_points, 0)
#    except:
#        P = 1
    # we are working on P points in N dimensions
    P, N = np.shape(non_grid_points)
    # The lagrangian functions in N dim are products of lagrangian functions 
    # in 1 D , so I work dimension-wise: we evaluate the multidimensional 
    # lagrange function on the corresponding column of points non_grid_points 
    # one dimension at a time and then we multiply.
    lagrange_monodim_eval = np.zeros((P, N))
    
    for dim in range(N):
        # These are the knots where the lagrange function is zero. I compute it
        # discarding the knot where I'm centering the multidim. lagr. function.
        # This is an expensive way, with setdiff:
        # monodim_knots = setdiff(knots_per_dim{dim},current_knot(dim));
        # This is much cheaper, with logical indexing.
        monodim_knots_temp = np.array(knots_per_dim[dim])
        monodim_knots = monodim_knots_temp[monodim_knots_temp != current_knot[dim]]
        
        # Now I compute the lagrange function in the dim-th dimension and evaluate
        # it in the points.
        lagrange_monodim_eval[:,dim] = lagr_eval(current_knot[dim], monodim_knots,
                                                 non_grid_points[:,dim])
        
    L = np.prod(lagrange_monodim_eval, axis = 1)
    return L

def lege_eval(x=None, k=None, a=None, b=None):
    """L = lege_eval(x,k,a,b)
    returns the values of the k-th Legendre polynomial orthoNORMAL in a,b w.r.t
    to rho=1/(b-a) in the points x (x can be a matrix as well).
    
    N.B. the polynomials start from k=0: L_0(x) = 1, L_1(x) = x
    """
    x = np.array(x)
    # This function expresses L as a function of the standard Legendre polynomial
    # (i.e. polynomials orthogonal in -1,1 w.r.t to rho=1 !! ), which are 
    # recursively calculated through the function standard_lege_eval, coded below
    # in this file.
    
    # first compute the transformation of x in (a,b) to t in (-1,1)
    t = np.divide(np.subtract(2.*x , a + b), b - a)
    
    # calculate the standard legendre polynomials in t
    L = standard_lege_eval(t,k)
    
    # modify L to take into account normalizations
    st_lege_norm = np.sqrt(2. / (2. * k + 1.))
    
    # moreover, add an additional sqrt(2) to take into account a general 
    # interval (a,b) , not (-1,1)
    L = np.sqrt(2.) * L / st_lege_norm
    return L

def standard_lege_eval(x=None, k=None):
    """L = standard_lege_eval(x)
    returns the values of the k-th standard Legendre polynomial 
    (i.e. polynomials orthogonal and not orthonormal in -1,1 w.r.t to rho=1 !! )
    in the points x ( x can be a vector as well)
    
    N.B. the polynomials start from k=0: L_0(x) = 1, L_1(x) = x
    """
    x = np.array(x)
    # base steps
    # read this as L(k-2)
    L_2 = np.ones(np.shape(x))
    # and this as L(k-1)
    L_1 = x
    
    if k == 0:
        L= L_2
    elif k == 1:
        L= L_1
    else:
        # recursive step
        for ric in range(2,k+1):
            L=(2. * ric - 1.) / ric * x*L_1 - (ric - 1.) / ric * L_2
            L_2=L_1
            L_1=L
    return L
    
def lege_eval_multidim(X=None, k=None, a=None, b=None):
    """L = LEGE_EVAL_MULTIDIM(X,k,a,b)
    evaluates the multidim Legendre polynomial order k (multi-index) orthonormal
    on [a,b]^N on the list of points X (each column is a point in R^N). 
    
    a,b may differ in each direction, so that actually the domain is not [a,b]^N,
    but [a1,b1] x [a2,b2] x [a3,b3] x ... [aN,bN]. In this case, a,b are defined
    as vectors, each with N components.
    """
    X = np.array(X)
    N, nb_pts = np.shape(X) # N here is the number of dimensions
    L = np.ones([1,nb_pts])
    # L is a product of N polynomials, one for each dim. I store the evaluation
    # of each of these polynomials as columns of a matrix

    M = 0. * X    
    
    # take care of the fact that a,b may be scalar values
    if np.size(a) == 1:
        a = a * np.ones(N)
        b = b * np.ones(N)
        
    for n in range(N):
        M[n,:] = lege_eval(X[n,:], k[n], a[n], b[n])
        
    L = np.prod(M,0)
    return L

def herm_eval_multidim(X=None, k=None, mu=None, sig=None):
    """H = herm_eval_multidim(X,k,mu,sig)
    evaluates the multidim Hermite polynomial order k (multi-index) orthonormal 
    on [-inf,+inf]^N with respect to 
    rho=prod_i 1/sqrt(2 pi sigma_i) * e^( -(x-mi_i)^2/(2*sigma_i^2) ) 
    on the list of points X (each column is a point in R^N). 
    mu,sig can be scalar values
    """
    X = np.array(X)
    N , nb_pts = np.shape(X) # N here is the number of dimensions
    H = np.ones([1,nb_pts])

    # take care of the fact that mu,sigma may be scalar values
    if np.size(mu) == 1:
        mu = mu * np.ones(N)
        sig = sig * np.ones(N)
        
    for n in range(N):
        H = H * herm_eval(X[n,:],k[n],mu[n],sig[n])
    return H

def herm_eval(x=None, k=None, mi=None, sigma=None):
    """H = herm_eval(x,k,mi,sigma)
    returns the values of the k-th Hermite polynomial orthoNORMAL in 
    (-inf,+inf) w.r.t to rho=1/sqrt(2 pi sigma) * e^( -(x-mi)^2/(2*sigma^2) )  
    in the points x (x can be a matrix as well).
    
    N.B. the polynomials start from k=0: L_0(x) = 1, L_1(x) = (x - mi)/sigma
    
    This function expresses H as a function of the standard Hermite 
    "probabilistic" polynomial (i.e. orthoGONAL w.r.t. 
    rho=1/sqrt(2 pi) * e^(-x^2/2) ), which are recursively calculated through 
    the function standard_herm_eval available in this file.
    """
    x = np.array(x)
    # first compute the transformation of x (referred to N(mi,sigma^2)) to z, 
    # the standard gaussian 
    z = (x - mi) / sigma
    H = standard_herm_eval(z,k)
    st_herm_norm = np.sqrt(np.math.factorial(k))
    H = H / st_herm_norm
    return H
    
def standard_herm_eval(x=None, k=None):
    """L = standard_herm_eval(x) 
    returns the values of the k-th standard Hermite "probabilistic" polynomial 
    (i.e. orthoGONAL w.r.t. rho=1/sqrt(2 pi) * e^(-x^2/2) ), in the points x 
    (x can be a vector as well).
    
    N.B. the polynomials start from k=0: L_0(x) = 1, L_1(x) = x
    """
    x = np.array(x)
    # read this as L(k-2)
    H_2 = np.ones(np.shape(x))
    # and this as L(k-1)
    H_1 = x
    
    if k == 0:
        H = H_2
    elif k == 1:
        H = H_1
    else:
        # recursive step
        for ric in range(2,k+1):
            H = x * H_1 - (ric - 1.) * H_2
            H_2 = H_1
            H_1 = H
    return H
    
def cheb_eval_multidim(X=None, k=None, a=None, b=None):
    """ C = cheb_eval_multidim(X,k,a,b)
    evaluates the multidim Chebyshev polynomial of the first kind of order k 
    (multi-index) on [a,b]^N on the list of points X (each column is a point 
    in R^N). a,b are scalar values a,b may differ in each direction, so that 
    actually the domain is not [a,b]^N, but [a1,b1] x [a2,b2] x [a3,b3] x ... [aN,bN] 
    in this case, a,b are defined as vectors, each with N components.
    """
    X = np.array(X)
    N,nb_pts=np.shape(X)
    C = np.ones([1,nb_pts])

    if np.size(a) == 1:
        a = a * np.ones(N)
        b = b * np.ones(N)
    for n in range(N):
        C = C*cheb_eval(X[n,:],k[n],a[n],b[n])
    return C


def cheb_eval(x=None, k=None, a=None, b=None):
    """ L = cheb_eval(x,k,a,b)
    returns the values of the k-th Chebyshev polynomial of the first kind 
    on [a,b] (i.e. T_n(a)=(-1)^n, T_n(b)=1), evaluated at x (vector).
    """
    x = np.array(x)
    # first compute the transformation of x in (a,b) to t in (-1,1)
    t = (2. * x - a - b) / (b - a)
    
    # base steps
    # read this as L(k-2)
    L_2 = np.ones(np.shape(t))
    L_1 = t
    if k == 0:
        L = L_2
    elif k == 1:
        L = L_1
    else:
        # recursive step
        for ric in range(2,k+1):
            L = 2.*t*L_1 - L_2
            L_2 = L_1
            L_1 = L
    return L

