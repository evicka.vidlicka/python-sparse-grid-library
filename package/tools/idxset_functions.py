import numpy as np
from ..src.mysortrows import mysortrows
from ..src.multiidx_gen import multiidx_gen
from .lev2knots_functions import lev2knots_lin, lev2knots_doubling, lev2knots_2step
import copy

def needed_set(idx):
    """Computes the indices of the form idx-e_j where e_j is the j-th 
    N-dimensional unit vector and store them as rows of S.
    """
    idx = np.reshape(np.array(idx), (1,-1))
    N = np.size(idx,1)
    
    # I can build the set quickly with matrices operation: [idx; idx; ... ; idx] - eye(N)
    S = np.dot(np.ones((N,1)), np.reshape(idx, (1,N))) - np.identity(N)
    
    # if idx has 1 inside, like [2 1 1], [3 1 2] etc, care has to be taken: 
    # the minimum value for indices is 1, so I don't have to check for [2 0 1],
    # [2 1 0] etc to be in the set. I handle this deleting all rows that contain 0.
    # Note that 0 can be only in the main diagonal of needed_set.
    D = np.diag(S)
    nnz_D = np.nonzero(D)[0]
    back_neigh = S[nnz_D,:].astype(int)#.tolist()
        
    return back_neigh
    
def array_diff(array1, array2):
    """Computes the difference of 2 arrays. Substitutes the Matlab function 
    "setdiff" using the Python function "setdiff1d".
    """
    if array1 == []:
        return np.array(array2)
    elif array2 == []:
        return np.array(array1)

    array1 = np.array(array1)
    array2 = np.array(array2)
    if np.size(array1, 1) != np.size(array2, 1):
        print("Can't make difference, not equal dimensions of multiindices")
        return False
    else:
        a1_rows = array1.view([('', array1.dtype)] * array1.shape[1])
        a2_rows = array2.view([('', array2.dtype)] * array2.shape[1])
        return np.setdiff1d(a1_rows, a2_rows).view(array1.dtype).reshape(
                -1,array1.shape[1])
            

def check_index_admissibility(idx, idx_set, sort_option=1):
    """[is_adm, completed_set, missing_set] = check_index_admissibility(idx, idx_set)
    given a multiindex idx as row vector checks if it is admissible w.r.t. 
    the index set idx_set (matrix with indices as rows). If it is so, 
    is_adm = true. Otherwise is_adm = false and the function will return the 
    completed_set and a list of the indices added stored in missing_set both 
    sorted in a lexicographic order. Note that the starting idx_set is supposed
    to be admissible, so its multi-indices will not be checked for admissibility. 
    
    [is_adm, completed_set, missing_set] = check_index_admissibility(idx, idx_set, 0)
    returns completed_set and missing_set not sorted in lexicographic order.
    """    
    is_adm = True
    
    # recast arrays as lists
    if isinstance(idx_set, list):
        pass
    else:
        idx_set = idx_set.tolist()
    
    completed_set = idx_set
    missing_set = []
    
    if np.size(np.shape(idx)) > 1 and np.shape(idx)[0] == 1:
        idx = idx[0,:]
    
    if type(idx) is np.ndarray:
        idx = idx.tolist()
    
    # now check if idx is admissible. Note that if this is not the case, the indices
    # needed may not be included in idx_set, too ! So we add everything we need to 
    # check in a queue and keep on checking while the queue is empty
        
    # here's the queue
    the_queue = [idx]
    
    while len(the_queue) > 0:
        
        # consider the first element of the stack
        i = the_queue[0]
        
        # build its needed set
        S = needed_set(i)

        # take the array_diff with idx_set. needed_rows is s.t. 
        # missing=needed_set(needed_rows,:)
        missing = array_diff(S, idx_set).tolist()
        
        # if missing is not empty
        if len(missing) > 0:
            
            # the initial set was not admissible
            is_adm = False
            
            # add missing to completed_set
            tmp_set = np.array(completed_set + missing)
            _, tmp_idx = np.unique(tmp_set, return_index = True, axis = 0)
            completed_set = tmp_set[np.sort(tmp_idx)].tolist()
            
            # add missing to missing set
            tmp_set = np.array(missing_set + missing)
            _, tmp_idx = np.unique(tmp_set, return_index = True, axis = 0)
            missing_set = tmp_set[np.sort(tmp_idx)].tolist()
            
            # add missing to the queue
            tmp_set = np.array(the_queue + missing)
            _, tmp_idx = np.unique(tmp_set, return_index = True, axis = 0)
            the_queue = tmp_set[np.sort(tmp_idx)].tolist()
            
        # delete current i from the queue
        del the_queue[0]
        
    # if requested, sort in ascending lexicographic order
    if sort_option != 0:
        if len(missing_set) > 0:
            missing_set = mysortrows(np.array(missing_set))[0].tolist()
        completed_set = mysortrows(np.array(completed_set))[0].tolist()
    
    return is_adm, completed_set, missing_set

def define_functions_for_rule(rule, input2):
    """[lev2nodes,idxset] = define_functions_for_rule(rule,N) 
    sets the functions lev2nodes and idxset to use in smolyak_grid() to build the
    desired ISOTROPIC sparse grid. N is the number of variables.
    
    [lev2nodes,idxset] = define_functions_for_rule(rule,rates)
    sets the functions lev2nodes and idxset to use in smolyak_grid() to build the
    desired ANISOTROPIC sparse grid with specified rates. 
    
    rule can be any of the following: 'TP', 'TD', 'HC', 'SM', 'TO'.
    The outputs are anonymous function , lev2nodes =@(i) ... and idxset=@(i) ...
    """        
    if np.isscalar(input2):
        N=input2
        rates=np.ones(N)
    else:
        rates=np.array(input2)
        
    if rule == 'TP':
        lev2nodes = lev2knots_lin
        idxset = lambda i: toarray(np.max(np.multiply(rates[0:np.size(i)],(i - 1))))
    elif rule == 'TD':
        lev2nodes = lev2knots_lin
        idxset = lambda i: toarray(np.sum(np.multiply(rates[0:np.size(i)],(i - 1))))
    elif rule == 'HC':
        lev2nodes = lev2knots_lin
        idxset = lambda i: toarray(np.prod(np.power(i,rates[0:np.size(i)])))
    elif rule == 'SM':
        lev2nodes = lev2knots_doubling
        idxset = lambda i: toarray(np.sum(np.multiply(rates[0:np.size(i)],(i - 1))))
    elif rule == 'TO':
        lev2nodes = lev2knots_2step
        idxset = lambda i: toarray(np.sum(np.multiply(rates[0:np.size(i)],(i - 1))))
    return lev2nodes,idxset

def toarray(x):
    """Return numpy array of list or scalar."""
    return np.array([x] if np.isscalar(x) else x)

def multiidx_box_set(shape, min_idx):
    """[C_with,C_without] = multiidx_box_set(ii,min_idx)
    given an index ii, generates C_with, the box indeces set up to that ii. 
    min_idx is either 0 or 1. C_without is C_with without ii itself
    """
    shape = np.array(shape)
    N = np.size(shape)
    shape_fun = lambda i: np.array(i) - shape[0:np.size(i)]
    C_with = multiidx_gen(N, shape_fun, 0, min_idx)   
    C_without = copy.copy(C_with[0:-1])
    return C_with, C_without

def check_set_admissibality(I):
    """[adm,C] = check_set_admissibality(I) checks whether I is an admissible 
    set. If that is the case, adm=true and C contains I ordered in lexicographic
    order. If not, adm=false and C contains I plus the multiindeces needed, 
    again in lexicographic order.
    """
    C = copy.copy(I)

    # now check amdissibility condition and add what's missing. Print a warning if needed
    row = 0
    row_max = np.size(np.array(C),0)
    adm = True
    
    while row<row_max:        
        # current index
        idx = C[row]
        is_adm, _, missing_set = check_index_admissibility(idx,C)
        
        # if it is not admissible, add what's needed to the bottom of C
        # and increase row_max, so that the new indices will also be checked.
        # So we don't sort the C after the update, otherwise I am not sure I am
        # checking everything
        if not is_adm:
            # update C and counter
            C = C + missing_set
            row_max = np.size(np.array(C),1)
            
            # print a warning
            print('the set is not admissible. Adding: ', missing_set) 
    
            # record non admissibility has been found
            adm = False
        row = row + 1
            
    # finally, sort C
    C = mysortrows(C)[0].tolist()
    
    return adm, C
    
    
    
    
    
    

