from ..data_structures import TensorGrid

def istensor(S):
    """returns 1 if S is a tensor grid. A tensor grid is an object of a class 
    with fields 'knots','weights','size','knots_per_dim','m'.
    """
    return int(isinstance(S, TensorGrid))