from ..data_structures import ReducedSG

def asreduced(pts_list, wgs_list = None):
    """S = asreduced(pts_list) returns a reduced sparse grids structure whose 
    "knots" field is set equal to pts_list, "size" filed is set to the number 
    of columns of pts_list, while the other fields are []. Thus, 
    isreduced(S)==true and S can then be used as input to e.g. PLOT_GRID or 
    EVALUATE_ON_SPARSE_GRID. The knots in pts_list are supposed to be all 
    different, i.e. the list is not checked for duplicates.
    
    S = asreduced(pts_list,wgs_list) also sets the weights field of S as 
    wgs_list. An error is thrown if the number of weights and nodes is not 
    identical.
    """

    S = ReducedSG()
    S.knots = pts_list
    S.weights = wgs_list
    S.m = None
    S.n = None
    return S