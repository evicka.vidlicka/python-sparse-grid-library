import numpy as np
from ..data_structures import ModTens
from ..tools.idxset_functions import multiidx_box_set
from ..tools.polynomials_functions import lege_eval_multidim, \
        herm_eval_multidim, cheb_eval_multidim, lege_eval, herm_eval, cheb_eval

def compute_modal_tensor(S=None, S_values=None, domain=None, flags=None):
    """given a tensor grid and the values on it, re-express the interpolant 
    as a modal expansion.
    
    U = COMPUTE_MODAL_TENSOR(S,S_VALUES,DOMAIN,'legendre') considers the 
    tensor grid S on the hyper-rectangle DOMAIN with associated point 
    evaluations S_VALUES and converts the resulting lagrangian multivariate 
    interpolant to a sum of Legendre polynomials. DOMAIN is a 2xN matrix 
    [a1, a2, a3, ...; b1, b2, b3, ...] defining the hyper-rectangluar domain 
    of the sparse grid: (a1,b1) x (a2,b2) x ... U is a struct with fields 
    U.size (the number of Legendre polynomials needed), U.multi_indices 
    (one multi-index per Legendre polynomial), U.coeffs.
    
    U=COMPUTE_MODAL_TENSOR(S,S_values,domain,'chebyshev') works as the previous
    call, using Chebyshev polynomials.
    
    U=COMPUTE_MODAL_TENSOR(S,S_values,domain,'hermite') works as the previous 
    call, using Hermite polynomials. Here DOMAIN is a 2XN matrix 
    [mu1, mu2, mu3, ...; sigma1, sigma2, sigma3,...] such that the first 
    variable has normal distribution with mean mu1 and std sigma1 and so on.
    
    U=COMPUTE_MODAL_TENSOR(S,S_values,domain,{<family1>,<family2>,<family3>,...})
    works as the previous call, using polynomials of type <family-n> in 
    direction n. Here DOMAIN is a 2XN matrix where each column gives the 
    parameters of the n-th family of polynomials (a,b for legendre, mu,sig for 
    hermite...)
    """

    nb_dim = np.size(S.knots,0)
    # I will need the knots in each dimension separately. 
    knots_per_dim = S.knots_per_dim 
    
    #for dim in range(nb_dim):
    #    knots_per_dim[dim]=np.unique(S.knots[dim,:])    
       
    # The modal expansion in i-th direction uses up to degree k, with k as 
    # follows.
    degrees = np.zeros(nb_dim)
    for dim in range(nb_dim):
        degrees[dim] = np.size(knots_per_dim[dim])-1
        
    I,_ = multiidx_box_set(degrees, 0)

    U = ModTens()
    U.multi_indices = I   
    nb_multiindices = np.size(I,0)
    U.size = nb_multiindices
    
    # Safety check. I have to solve a system, so I need the vandermonde matrix 
    # to be squared!
    rows = S.size

    cols = nb_multiindices
    if rows != cols:
        raise ValueError('SparseGKit:FailedSanityChk','vandermonde matrix will not be square!')
        
    # now create the vandermonde matrix with evaluation of each multi-index in 
    # every point of the grid
        
    V = np.zeros((rows,cols))
    if not isinstance(flags, list):
        flags = [flags]
    if len(flags) == 1:
        for c in range(cols):
            k = I[c,:]
            if flags[0] == 'legendre':
                vc = lege_eval_multidim(S.knots,k,domain[0,:],domain[1,:])
            elif flags[0] == 'hermite':
                vc = herm_eval_multidim(S.knots,k,domain[0,:],domain[1,:])
            elif flags[0] == 'chebyshev':
                vc = cheb_eval_multidim(S.knots,k,domain[0,:],domain[1,:])
            else:
                 print('unknown family of polynomials')
            V[:,c] = vc
    else:
        for c in range(cols):
            k = I[c,:]
            vc = np.ones((1,np.size(S.knots,1)))
            for n in range(nb_dim):
                if flags[n] == 'legendre':
                    vc = vc * lege_eval(S.knots[n,:],k[n],domain[0,n],domain[1,n])
                elif flags[n] == 'hermite':
                    vc = vc * herm_eval(S.knots[n,:],k[n],domain[0,n],domain[1,n])
                elif flags[n] == 'chebyshev':
                    vc = vc * cheb_eval(S.knots[n,:],k[n],domain[0,n],domain[1,n])
                else:
                    print('unknown family of polynomials')
            V[:,c] = vc
    U.modal_coeff = np.linalg.solve(V,S_values)
    return U