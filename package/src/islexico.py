import numpy as np

def islexico(a, b):
    """islexico checks lexicographic order of vectors islexico(A,B) for A and B
    vectors returns logical 1 (true) if A<=B in lexicographic sense.
    
    Examples:

     islexico([1 2 3],[1 2 5]) -> true  
     islexico([1 2 3],[1 2 3]) -> true
     islexico([1 2 3],[1 2 1]) -> false
     islexico([2 2 3],[1 7 1]) -> false
     islexico([1 7 3],[1 5 3]) -> false
    """

    v = np.nonzero(a-b)[0][0]
    if np.size(v) == 0 or (a-b)[v] < 0:
        return True
    else:
        return False
