import matplotlib.pyplot as plt

def plot_idx_status(G, I, idx_bin, idx):
    """plots status of a two-dimensional adaptive sparse grid."""
    # plot the indices used
    plt.figure()
    plt.plot(G[:,0], G[:,1], 'xr', linewidth=2, markersize=14, 
             label='G -- set used to build the sparse grid')
    plt.plot(I[:,0], I[:,1], 'o', markerfacecolor='blue', markersize=8,
             label='I -- set of idx whose neighbour has been explored')
    if len(idx_bin)!= 0:
        plt.plot(idx_bin[:,0], idx_bin[:,1], 'sk', markerfacecolor='None', 
                 markersize=14, label='idx-bin -- set of idx whose neighbour',
                 'has not been explored')
    plt.plot(idx[0], idx[1], 'og', markerfacecolor='g', markersize=8, 
             label='next idx to be considered')
    
    plt.legend()
    plt.show()