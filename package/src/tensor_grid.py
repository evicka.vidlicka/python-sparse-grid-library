import numpy as np
from ..data_structures import TensorGrid
import itertools
import warnings

def tensor_grid(N, m, knots):
    """generates a tensor grid and computes the corresponding weights.
    
    S = tensor_grid(N,M,KNOTS) creates a tensor grid in N dimensions with 
    M=[m1,m2,...,m_N] points in each direction. KNOTS is either a cell array 
    containing the functions to be used to generate the knots in each direction,
    i.e.
                KNOTS={@knots_function1, @knots_function2, ... }
    
    or a single function, to be used to generate the 1D knots in every 
    direction, i.e.
   
               KNOTS=@knots_function1
    
    In both cases, the header of knots_function is [x,w]=knots_function(m)
    
    The output S is an instance of a class containing the information on the 
    tensor grid:
    S.knots: vector containing the tensor grid knots
    S.weights: vector containing the corresponding weights
    S.size: size of the tensor grid, S.size = prod(m)
    S.knots_per_dim: list of length N, each element is an array of 1D knots 
                        used to build the tensor grid
    S.m: input vector m, m(i) = len(S.knots_per_dim[i])
    """
    # if knots is a simple function, we replicate it in a cell
    if hasattr(knots, '__call__'):
        knots = [knots]*N
        
    if N != len(m):
        warnings.warn('SparseGKit: Considered dimension N differs from the'\
                      'dimension of the demanded points -- points will duplicate.')
    sz = int(np.prod(m))
    S = TensorGrid()
    S.knots = np.zeros((N, sz))
    S.weights = np.ones(sz)
    S.size = sz
    S.knots_per_dim = [None]*N
    S.m = m
    # generate the pattern that will be used for knots and weights matrices, e.g.
    #
    # pattern = [1 1 1 1 2 2 2 2;
    #            1 1 2 2 1 1 2 2;
    #            1 2 1 2 1 2 1 2]
    #
    # meaning "first node d-dim uses node 1 in direction 1, 2 and 3, second 
    # d-dim  node uses node 1 in direction 1 and 2 and node 2 in direction 3 ...
    pattern = np.array([s for s in itertools.product(
            *[list(range(1,j+1)) for j in m])], dtype=np.int).transpose()
    
    for n in range(N):
        xx, ww = knots[n](m[n])
        S.knots[n, :] = toarray(xx)[pattern[n, :]-1]
        S.weights = S.weights*toarray(ww)[pattern[n, :]-1]
        S.knots_per_dim[n] = xx
    return S

def toarray(x):
    """Return numpy array of list or scalar."""
    return np.array([x] if np.isscalar(x) else x)






