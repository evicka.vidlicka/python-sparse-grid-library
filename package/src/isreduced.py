from ..data_structures import ReducedSG

def isreduced(S):
    """returns 1 if S is a reduced sparse grid. A reduced sparse grid is an 
    object of a class with fields 'knots','m','weights','n','size'.
    """
    return int(isinstance(S, ReducedSG))