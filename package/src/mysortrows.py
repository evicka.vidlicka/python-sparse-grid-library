import numpy as np

def mysortrows(A, Tol=1.e-14, i=None, i_ord=None, n=0):
    """Similar to Matlab builtin function sortrows. Given a matrix A of real 
    numbers, sorts the rows in lexicographic order; entries that differ less 
    than Tol are treated as equal (default Tol is 1e-14). 
    
    usage:
       [B,i]=mysortrows(A,Tol)
       input
         A: input matrix
         Tol: (optional) tolerance used to identify coincident entries
              (default 1e-14)
       output
         B: sorted matrix
         i: index vector such that A(i,:)=B
    
    Mysortrows has a recursive implementation.
    recursive call: [A,i_ord]=mysortrows(A,Tol,i,i_ord,n) (with i index)
    sorts the submatrix A(i,n:end); modifies the matrix itself and
    stores the rows permutation in the global vector i_ord.
    """
    if A == [] or np.size(A) == 0:
        return np.array([]), []
    A = np.array(A)
    if i is None:
        i = np.arange(0, A.shape[0], dtype=np.int)
    if i_ord is None:
        i_ord = i.copy()
    ii = np.argsort(A[i, n])
    A[i, :] = A[i[ii], :]
    i_ord[i] = i_ord[i[ii]]

    if n < A.shape[1]-1:
        j = np.concatenate(([1], np.diff(A[i, n]) > Tol, [1]))
        jm, = np.nonzero(np.diff(j) == -1)
        jp, = np.nonzero(np.diff(j) == 1)
        for k in range(0, jm.shape[0]):
            v = i[jm[k]:jp[k]+1]
            A, i_ord = mysortrows(A, Tol, v, i_ord, n+1)
    return A, i_ord
