from ..data_structures import TensorGrid

def issmolyak(S):
    """returns 1 if S is a smolyak sparse grid. A smolyak sparse grid is a
    list of objects of a class with fields 'knots','weights','size',
    'knots_per_dim','m','coeff','idx'.
    """
    if isinstance(S,list):
        if all(isinstance(x, TensorGrid) for x in S):
            return 1
        else:
            return 0
    else:
        return 0
