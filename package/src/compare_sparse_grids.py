import numpy as np

def compare_sparse_grids(S,Sr,S_old,Sr_old,Tol = 1e-14, verbose = True):
    """Compares the points of two sparse grids.
    
    Compares the points in S and S_old, and determines those in common, and 
    those belonging exclusively to each of the two. Points are considered equal
    if coordinate-wise they are closer than Tol. SR is the reduced version of S
    and Sr_old is the reduced version of S_old. 
    
    The function returns 4 sets of scalar indices, which refer to the columns 
    of Sr.knots and Sr_old.knots. In this way, each point of Sr.knots and 
    Sr_old.knots is catalogued as follows:
        
    1) in_S_only contains the column indices of points of Sr.knots that are not 
    in the intersection of S and S_old.
    
    2) in_both_grids_S are the column indices of the points of Sr.knots that 
    are also in Sr_old.knots.
    
    3) in_both_grids_S_old are the column indices of the points of Sr_old.knots
    that are also in Sr.knotsS, i.e.
    Sr.knots[:,in_both_grids_S] == Sr_old.knots[:,in_both_grids_S_old]
    
    4) in_S_old_only are column indices of the points of Sr_old.knots that are 
    in Sr_old but not in Sr.
    
    Example: 
    
    Suppose
    
    Sr.knots = [[a, b, c, d, e],
                [x, y, z, w, t]]
    
    Sr_old.knots = [[a, f, b, h],
                    [x, s, y, r]]
    
    Then: 
    
    in_S_only             = [2,3,4];
    in_both_grids_S       = [0,1];
    in_both_grids_S_old   = [0,2];
    in_S_old_only         = [1,3];
    
    Crucially, the same results would happen if Sr_old.knots are perturbed by 
    numerical noise below Tol
    
    Sr_old.knots = [[a+n, f, b, h]
                    [x, s, y-n,  r]]
    
    compare_sparse_grids is very efficient because it works with comparing 
    integer indices coming from the multiindices in S.idx and S_old.idx as much
    as possible. See also lookup_merge_and_diff for the same kind of analysis
    based however on comparing the actual coordinates of the points.
    """
    # =========================================================================
    #  PART I: looking for MULTI-IDX 
    # =========================================================================
    # To get good performance, we proceed by tensor grids: we identify which 
    # tensor grids are in common between S and S_old, and we classify the 
    # points of the two sparse grids depending whether they belong to tensor 
    # grids in common between the two sparse grids or not. Thus, the first 
    # thing to do is to recover the set of multiindices corresponding to the 
    # tensor grids stored in S and S_old. Note that these are subsets of the 
    # index sets that generated the grids (i.e, they are only those who 
    # "survived" the combination techinque selection).

    N = np.size(Sr.knots, 0)
    N_old = np.size(Sr_old.knots, 0)
    if N != N_old:
        raise ValueError('SparseGKit:FailedSanityChk','grids with different N')
        
    nb_idx_S = len(S)
    I_S = np.array([a.idx for a in S]).T
    
    nb_idx_S_old = len(S_old)
    I_S_old = np.array([a.idx for a in S_old]).T
    
    # We now need to identify which multiindices (hence tensor grids) belongs 
    # to both sparse grids and which are in one grid only. To do so, we exploit
    # the fact that I_S and I_S_old are lexicographically ordered: we pick the
    # smallest of the two sets, and we look for all of its multiidx in the 
    # other set. We then generate 4 vectors of indices that refer to the 
    # elements of I_S and I_S_old:
    # idx_in_both
    # idx_in_both_old
    # idx_in_S_only
    # idx_in_S_old_only
    
    
    I_merged = np.concatenate((I_S, I_S_old), axis = 1)
    
    un, ind, inv, count = np.unique(I_merged, return_index=1, return_inverse=1,
                                    return_counts=1, axis = 1)
    mult_ind = np.where(count > 1)[0]
    idx_in_both = np.zeros(np.size(mult_ind), dtype = int)
    idx_in_both_old = np.zeros(np.size(mult_ind), dtype = int)
    for i in range(np.size(mult_ind)):
        tmp_where = np.where(inv == mult_ind[i])[0]
        idx_in_both[i] = tmp_where[0]
        idx_in_both_old[i] = tmp_where[1] - nb_idx_S
    idx_in_S_only = np.setdiff1d(np.arange(nb_idx_S), idx_in_both)
    idx_in_S_old_only = np.setdiff1d(np.arange(nb_idx_S_old), idx_in_both_old)
    
    
    # =========================================================================
    #  PART II: classifying points based on their origin
    # =========================================================================
    # Now, we derive information about sparse grid points (i.e. whether points 
    # are in S, S_old, or both) from the multiindex information. At the end of 
    # this part, we will have split points in 4 categories, two for points in S
    # and two for points in S_old
    # pts_from_new_idx_in_Sr
    # pts_from_common_idx_in_Sr 
    # pts_from_common_idx_in_Sr_old
    # pts_from_old_idx_in_Sr_old
    
    # ----------------------------- part II.a ---------------------------------
    # We begin by noting that points that belong to tensor grids associated to 
    # multiidx in both sparse grids will be necessarily present in both grids 
    # and we will return them as "in_both_grids". For now, we collect their 
    # position in both grids, Sr and Sr_old, i.e. in which column of Sr.knots 
    # and Sr_old.knots they are.
    
    # We start from Sr. This is the cointainer. Note that it will be not sorted
    # at first, hence the name. We will sort it afterwards.
    
    if not np.size(idx_in_both) == 0:
        pts_from_common_idx_in_Sr_nosort = []
        pos_end = -1
        i_prev = -1
        # For each multiidx, we recover where the points of the associated 
        # tensor grid have been placed in Sr, thanks to the vector Sr.n, that 
        # says precisely this information. Of course, grids in common and not 
        # in common are interwined in Sr, so we need to use the S(i).size 
        # information to move to the correct fragment of Sr.n
        for i in idx_in_both.tolist():
            pos = pos_end + sum([S[j].size for j in range(i_prev+1,i)]) + 1
            pos_end = pos + S[i].size - 1
            pts_from_common_idx_in_Sr_nosort += Sr.n[pos:pos_end+1].tolist()
            i_prev = i
        # The vector pts_from_common_idx_in_red_nosort is not uniqued, because 
        # points of different tensor grids might be mapped to the same point in
        # Sr. We need them uniqued though, but the official unique is slow, so
        # we reimplement it as follows:
        vs = np.sort(np.array(pts_from_common_idx_in_Sr_nosort))
        vd = np.diff(vs)
        pts_from_common_idx_in_Sr = np.append(vs[np.where(vd > 0)[0]], vs[-1])
    else:
        pts_from_common_idx_in_Sr = np.array([])
        
    print('pts_from_common_idx_in_Sr')
    print(pts_from_common_idx_in_Sr)
        
    # same procedure to locate the common points in SR_old starting from the common indices
    if not np.size(idx_in_both_old) == 0:
        pts_from_common_idx_in_Sr_old_nosort=[]
        pos_end = -1
        i_prev = -1
        for i in idx_in_both_old.tolist():
            pos = pos_end + sum([S_old[j].size for j in range(i_prev+1,i)]) + 1
            pos_end = pos + S_old[i].size - 1
            pts_from_common_idx_in_Sr_old_nosort += Sr_old.n[pos:pos_end+1].tolist()
            i_prev = i
        vs = np.sort(np.array(pts_from_common_idx_in_Sr_old_nosort))
        vd = np.diff(vs)
        pts_from_common_idx_in_Sr_old = np.append(vs[np.where(vd > 0)[0]], vs[-1])
    else:
        pts_from_common_idx_in_Sr_old = np.array([])
    
    print('pts_from_common_idx_in_Sr_old')
    print(pts_from_common_idx_in_Sr_old)
        
    # ----------------------------- part II.b ---------------------------------
    # Next, we perform the complementary operation and identify the column 
    # index of the points in Sr corresponding to multiidx in S only, and same 
    # for S_old. Note that it's the multiidx that belongs to S only, not the 
    # point!  If the 1D knots have some degree of nestedness, it will actually 
    # happen that some of the points of multiidx in S_only will also be present
    # in grid of midx in common. Pay attention however, it might happen that 
    # idx_in_S_only is empty, for instance if the "old" grid is actually larger.
    # In this case, we throw an error.
    
    if np.size(idx_in_S_only) == 0:
        raise ValueError('SparseGKit:ToBeCoded',
                         'idx_in_S_only is empty: this case is not handled yet')
    else:
        pts_from_new_idx_in_Sr_nosort = []
        pos_end = -1
        i_prev = -1
        for i in idx_in_S_only.tolist():
            pos = pos_end + sum([S[j].size for j in range(i_prev+1,i)]) + 1
            pos_end = pos+S[i].size-1
            pts_from_new_idx_in_Sr_nosort += Sr.n[pos:pos_end+1].tolist()
            i_prev = i
        vs = np.sort(np.array(pts_from_new_idx_in_Sr_nosort))
        vd = np.diff(vs)
        pts_from_new_idx_in_Sr = np.append(vs[np.where(vd > 0)[0]], vs[-1])
        
    print('pts_from_new_idx_in_Sr')
    print(pts_from_new_idx_in_Sr)
        
    # Now, the same for S_old, i.e. the column index of the points in Sr_old 
    # corresponding to multiidx in S_old only. Pay attention however, it might 
    # happen that idx_in_S_old_only is empty ...
    
    if not np.size(idx_in_S_old_only) == 0:
        pts_from_old_idx_in_Sr_old_nosort = []
        for i in idx_in_S_old_only.tolist():
            pos = sum([S_old[j].size for j in range(i)])
            pos_end = pos + S_old[i].size-1
            pts_from_old_idx_in_Sr_old_nosort += Sr_old.n[pos:pos_end+1].tolist()
        vs = np.sort(np.array(pts_from_old_idx_in_Sr_old_nosort))
        vd = np.diff(vs)
        pts_from_old_idx_in_Sr_old = np.append(vs[np.where(vd > 0)[0]], vs[-1])
        
    print('pts_from_old_idx_in_Sr_old')
    print(pts_from_old_idx_in_Sr_old)
    
    # =========================================================================
    #  PART III: preparing the final outputs
    # =========================================================================
    # Here, we start from these vectors:
    #
    # -- pts_from_new_idx_in_Sr
    # -- pts_from_common_idx_in_Sr 
    # -- pts_from_common_idx_in_Sr_old
    # -- pts_from_old_idx_in_Sr_old
    #
    # and at the end of this section, we'll have points categorized in the 
    # final lists:
    #
    # -- pts_in_S_only
    # -- pts_in_both_grids_S,
    # -- pts_in_both_grids_S_old,
    # -- pts_in_S_old_only
    
    # We'd like to proceed differently whether the points are nested or not, 
    # but that's not as easy as it seems so we do not take this into account.
        
    # As we have said already points from new idx might need be new, but not 
    # necessarily. Indeed:
    # --> they might again be in the common points (imagine adding [5x1] grid 
    # to a [3x1]. the new index [5x1] is new,
    # but some of its points might be in the [3x1] - actually, all of them if
    # nested points!
    # --> they might be among the points of the idx we are trashing
    # at the same time, points from old idx might also be in points from common
    #idx!
    #
    # So, this is the strategy:
    # before trashing points, we check if they are also in common. Otherwise, 
    #before trashing them, we make sure that they are not needed as points from
    # new idx.
    
    #print(pts_from_new_idx_in_Sr)
    #print(pts_from_old_idx_in_Sr_old)
    
    if np.size(idx_in_S_old_only) == 0:
        # If we are not trashing multiidx, then we init the set of new points 
        # as the set of points coming from new idx, we'll figure out soon which
        # points are not really new ...  the rest of the outputs are empty for 
        # now.
        pts_in_S_only = pts_from_new_idx_in_Sr
        pts_to_recycle_from_S_old_only_list = np.array([])
        pts_to_recycle_from_S_old_only_list_old = np.array([])
        pts_to_discard_from_S_old_list = np.array([])
    else:
        # First, we make sure that points coming from old idx in Sr_old are 
        # really going to be discarded, or if they are also in points in common.
        pts_from_old_idx_in_Sr_old = np.setdiff1d(pts_from_old_idx_in_Sr_old, 
                                                  pts_from_common_idx_in_Sr_old)
        # now, depending whether pts_from_old_idx_in_Sr_old is still not empty 
        # or not, we behave differently
        if not np.size(pts_from_old_idx_in_Sr_old) == 0:
            # If they really are going to be discarded, let's see if perhaps we
            # can use them in the points of the new grid coming from new idx. 
            # Here we need to resort to comparing the coordinates ...
            if verbose:
                print('(hopefully) small local comparison between coordinates',
                      'of points...')
                pts_list = Sr.knots[:, pts_from_new_idx_in_Sr].T
                pts_list_old = Sr_old.knots[:, pts_from_old_idx_in_Sr_old].T
                
                # Do the comparison. Observe that the output give the position 
                # of the common points in the subparts of Sr.knots and 
                # Sr_old.knots, so we need them to take them back to the 
                # "global" counting.
        
    
    return 0
    





    #    
#    
#    
    
    
    
        
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
#    pts_in_both_grids_S_old = list(range(np.size(Sr_old.knots, 1)))
#    pts_in_S_old_only = [];
#    pts_in_both_grids_S = []
#    
#    dim = np.size(Sr.knots[:,0])
#    dim_old = np.size(Sr_old.knots[:,0])
#    
#    if dim > dim_old:
#        tmp = np.zeros((dim-dim_old, np.size(Sr_old.knots, 1)))
#        knots_old_added = np.concatenate((Sr_old.knots, tmp), axis = 0)
#    else:
#        knots_old_added = Sr_old.knots
#        
#    tmp = np.concatenate((np.around(Sr.knots, decimals = 10), np.around(knots_old_added, decimals = 10)), axis = 1)
#            
#    #tmp = np.concatenate((Sr.knots, knots_old_added), axis = 1)
#    tmp_uni, counts = np.unique(tmp,return_counts = True, axis = 1)
#    pts_in_both_grids_S = np.where(counts > 1)[0].tolist()
#    
#    pts_in_S_only = list(set(range(np.size(Sr.knots, 1))) - set(pts_in_both_grids_S))
#    
#    return pts_in_S_only,pts_in_both_grids_S,pts_in_both_grids_S_old,pts_in_S_old_only