import numpy as np

def multiidx_gen(N, rule, w, base=0, multiidx=[], MULTI_IDX=[]):
    """calculates all multiindices M_I of length L with elements such that 
    rule(M_I) <= w.  M_I's are stored as rows of the matrix MULTI_IDX indices 
    will start from base (either 0 or 1).
    
    multiidx_gen works recursively, exploring in depth the tree of all possible
    multiindices. The current multi-index is passed as 5-th input argument, and
    eventually stored in MULTI_IDX. The starting point is the empty multiidx: 
    [], and MULTI_IDX is empty at the first call of the function.    
    """
    if len(multiidx) != N:
        # recursive step: generates all possible leaves from the current node 
        # (i.e. all multiindexes with length le+1 starting from the current 
        # multi_idx, which is of length le that are feasible w.r.t. rule)
        i = base
        while all(np.reshape(np.array(rule(toarray(multiidx + [i]))) <= w, (1,-1))[0,:]):
            # if [multiidx, i] is feasible further explore the branch
            # of the tree that comes out from it.
            MULTI_IDX = multiidx_gen(N, rule, w, base, multiidx + [i],
                                     MULTI_IDX)
            i = i+1
    else:
        # base step: if the length of the current multi-index is L
        # then I store it in MULTI_IDX (the check for feasibility was
        # performed in the previous call
        MULTI_IDX = MULTI_IDX + [multiidx]
    return MULTI_IDX

def toarray(x):
    """Return numpy array of list or scalar."""
    return np.array([x] if np.isscalar(x) else x)