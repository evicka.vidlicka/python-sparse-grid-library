import numpy as np
import warnings

def detect_insufficient_tolerance(pts, tol):
    """is_insuf = detect_unsufficient_tolerance(pts,tol)
    for a matrix of points (one per row, as in mysortrows or 
    lookup_merge_and_diff) computes an approximation of the support of the set 
    of point in each direction (as max - min coord in each dir) and compares 
    it with tol. It they are "same-sized" then tol is too large and a warning 
    is thrown.
    """
    N = np.shape(pts)[1]
    for n in np.arange(N):
        # check one column at a time. exploit the fact that unique returns a 
        # sorted sequence
        uu = np.unique(pts[:, n])
        # if the tolerance is not at least 2 order of magnitude smaller than 
        # the support on the n-th direction, throw a warning
        if np.size(uu) == 1:
            return False
        if np.abs(np.log10(uu[-1]-uu[0])-np.log10(tol)) < 2:
            warnings.warn('SparseGKit:TolNotEnough: tol does not seem small' ,
                          'enough to detect identical points:', uu[-1] , uu[0])
            return True
    return False