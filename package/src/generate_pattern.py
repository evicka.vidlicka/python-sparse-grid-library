import itertools
import numpy as np

def generate_pattern(m):
    """given m=[m1 m2 m3 m4 ... mN] generates a matrix that can be used to 
    generate the cartesian product of 
    {1,2,...,m1} x {1,2,...,m2} x {1,2,...m3} x ....
    e.g.
    
    generate_pattern([3 2 2])
    
    pattern =

      1  2  3  1  2  3  1  2  3  1  2  3
      1  1  1  2  2  2  1  1  1  2  2  2
      1  1  1  1  1  1  2  2  2  2  2  2
    """
    coords = []
    print(m)
    for i in range(len(m)-1,-1,-1):
        coords.append(range(1,m[i]+1))
        
    print(coords)
    
    it = itertools.product(*coords)
    
    return np.array(list(it)).T[::-1]