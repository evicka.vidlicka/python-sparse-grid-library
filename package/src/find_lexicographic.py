import numpy as np
import math
from .islexico import islexico
from .mysortrows import mysortrows



def find_lexicographic(lookfor, I, nocheck = 'True'):
    """find_lexicographic finds specific rows of a matrix that is sorted 
    lexicographically.
    
    [FOUND,POS] = find_lexicographic(LOOKFOR,I) looks for the row vector 
    LOOKFOR among the rows of I. If found, FOUND=TRUE and POS is the rownumber 
    of LOOKFOR, i.e. LOOKFOR == I(POS,:). If not found, FOUND=FALSE and POS=[]. 
    The function performs a preliminar check whether I is actually 
    lexicographically sorted.
    
    [FOUND,POS] = find_lexicographic(LOOKFOR,I,'False') is the same but does 
    *not* check whether I is actually lexicographically sorted. This could be 
    useful for speed purposes.
    """
    if not nocheck:
        I_sorted = mysortrows(I)[0]
        # if sorted I is the same as original I
        if not np.all(I == I_sorted):
            raise ValueError('SparseGKit:SetNotSorted',
                             'I is not lexicographically sorted')
    I = np.array(I)
    lookfor = np.array(lookfor)
    
    if np.size(I,0) == 0:
        return False, []
    
    if np.size(I[0]) != np.size(lookfor):
        return False, []
    
    nb_idx = np.size(I, 0)
    
    # We exploit the fact that I is sorted lexicographically and we proceed by 
    # binary search which guarantees cost ~ log(nb_idx).
    #
    # Basically, we start from the middle row, compare it with the index to be 
    # found, and if our index is larger we make a step in the increasing 
    # direction (i.e. we look in the upper half of the sorting), and viceversa.
    # Of course, the step halves at each iteration: therefore we necessarily 
    # terminate in ceil(log2(nb_idx)) steps at most.
    
    # the position to compare against -- if found, this is the position to be 
    # returned
    idx = int(math.ceil(nb_idx/2))-1
    # the associated vector 
    jj = I[idx]        
        
    found = equal_points(jj, lookfor)
    
    iterr = 1
    itermax = int(math.ceil(np.log2(nb_idx)))
    
    while not found and iterr <= itermax:
        if islexico(jj, lookfor):
            # look in the upper half, unless we are already at the end
            if idx < nb_idx:
                idx = min((idx+1) + int(math.ceil(nb_idx/math.pow(2,iterr))), nb_idx)-1
                jj = I[idx]      # row entry of C2          
                found = equal_points(lookfor, jj)
                iterr += 1
            else:
                break
        else:
            # look in the bottom half, unless we are already at the beginning
            if idx > 0:
                idx = max((idx+1) - int(math.ceil(nb_idx/math.pow(2, iterr))), 1)-1
                jj = I[idx]    # row entry of C2            
                found = equal_points(lookfor, jj)
                iterr += 1
            else:
                break
    pos = []
    if found:
        pos = idx
    return found, pos

def equal_points(a,b, tol = 1e-14):
    if np.size(a) != np.size(b):
        return False
    if np.abs(a - b).any() < tol:
        return True
    else:
        return False
