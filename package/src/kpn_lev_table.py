import numpy as np

def kpn_lev_table(rows, cols):
    M = np.array([[0, 0, 0, 0],
                  [1, 1, 1, 1],
                  [2, 3, 3, 3],
                  [3, 8, 9, 15],
                  [4, 15, 19, 29],
                  [5, 25, 35, 51]])
    return M[rows,cols]