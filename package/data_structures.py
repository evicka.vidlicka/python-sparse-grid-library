class TensorGrid(object):
    def __init__(self):
        self.knots = None
        self.weights = None
        self.size = None
        self.coeff = None
        self.m = None
        self.knots_per_dim = None
        self.idx = None
        
class Controls(object):
    def __init__(self):
        self.nested = True
        self.max_pts = 200
        self.prof_tol = 1e-14
        self.paral = None
        self.recycling = 'priority_to_evaluation'
        self.prof = 'Linf'
        self.op_vect = None
        self.pts_tol = None
        self.pdf = None
        self.var_buffer_size = 2
        self.plot = False
        
class Adapted(object):
    def __init__(self):
        self.S = None
        self.Sr = None
        self.S_stable = None
        self.Sr_stable = None
        self.f_on_Sr = None
        self.f_on_Sr_stable = None
        self.nb_pts = None
        self.nb_pts_visited = None
        self.num_evals = None
        self.N = None
        self.private = None
        
class Private(object):
    def __init__(self):
        self.G = None
        self.I = None
        self.I_log = None
        self.N_log = None
        self.idx = None
        self.maxprof = None
        self.profits = None
        self.idx_bin = None
        self.var_with_pts = None
        self.var_with_pts_stable = None
        self.nb_pts_log = None
        self.Hr = None
        self.f_on_Hr = None

class ReducedSG(object):
    def __init__(self):
        self.knots = None
        self.m = None
        self.weights = None
        self.n = None
        self.size = None
        
class ModTens(object):
    def __init__(self):
        self.size = None
        self.multi_indices = None
        self.modal_coeff = None