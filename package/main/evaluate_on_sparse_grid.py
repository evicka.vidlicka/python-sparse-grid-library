import numpy as np
from ..data_structures import ReducedSG
from ..src.lookup_merge_and_diff import lookup_merge_and_diff
from ..src.compare_sparse_grids import compare_sparse_grids

def evaluate_on_sparse_grid(f,S,Sr = None,evals_old = None,S_old = None,Sr_old = None,paral = None,tol = 1e-14):
    if Sr is None:
        if isinstance(S, ReducedSG):
            f_eval = simple_evaluate(f, S)
            new_points = S.knots
            tocomp_list = list(range(np.size(S.weights)))
            discard_points = []
            discard_list = []
            return f_eval, new_points, tocomp_list, discard_points, discard_list
        else:
            raise ValueError('when evaluate_on_sparse_grid is called with two inputs, the second one must be a reduced sparse grid')
    
    elif evals_old is None or S_old is None or Sr_old is None:
        raise ValueError('Observe that EVALUATE_ON_SPARSE_GRID has been changed after release 15.8')
    
    else:
        #if not isinstance(S, SparseGrid):
        #   raise ValueError('The second input of EVALUATE_ON_SPARSE_GRID must be a non-reduced sparse grid if the function is called with 6 inputs. ')
        if evals_old is None and S_old is None and Sr_old is None:
            f_eval = simple_evaluate(f, Sr)
            new_points = Sr.knots
            tocomp_list = list(range(np.size(Sr.weights)))
            discard_points = []
            discard_list = []
            return f_eval, new_points, tocomp_list, discard_points, discard_list
            
# we store the needed information in three vectors:
#
# -> tocomp_list contains the indices of the points where f needs to be evaluated
# -> recycle_list contains the indices of the points that have been already evaluated
# -> recycle_list_old contains their position in the old sparse grid
        
    if S_old is None: # here SR_OLD is matrix with points stored as columns, and we go for the slow code
        raise ValueError ('S_old empty, not implemented!!')
        #f_eval = simple
        pts_list = Sr.knots
        pts_list_old = Sr_old.knots
        tocomp_list, recycle_list, recycle_list_old, discard_list = lookup_merge_and_diff(pts_list,pts_list_old,tol)
    else: # here S_OLD is a sparse grid and SR_OLD is its reduced version and we go for the faster alternative
        pts_list = Sr.knots
        tocomp_list,recycle_list,recycle_list_old,discard_list = compare_sparse_grids(S,Sr,S_old,Sr_old,tol)
        
    
    new_points = [pts_list[:,a] for a in tocomp_list]
    discard_points = [pts_list_old[:,a] for a in discard_list]
    
    # evaluation
    
    N = np.size(pts_list, 1)
    s = np.size(evals_old, 0)
    

    f_eval = np.zeros((s, N))
    f_eval[:, tocomp_list] = f(pts_list[:,tocomp_list])
    f_eval[:, recycle_list] = evals_old[:, recycle_list_old]
       
    return f_eval, new_points, tocomp_list, discard_points, discard_list

def simple_evaluate(f,Sr):
    """Evaluate function f on a reduced sparse grid Sr. Does not exploit 
    previous sparse grids evaluations.
    """

    n = np.size(Sr.knots,1)
    
    # probe f to see the size of its output 
    probe = f(Sr.knots[:,0])
    output = np.zeros((np.size(probe),n))
    output[:,0] = probe
    output[:,1:n] = f(Sr.knots[:,1:n])
    return output





