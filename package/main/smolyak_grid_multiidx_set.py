import numpy as np
from ..src.mysortrows import mysortrows
import inspect as ins
from ..src.tensor_grid import tensor_grid
import copy
from ..src.find_lexicographic import find_lexicographic
from ..src.issmolyak import issmolyak

def smolyak_grid_multiidx_set(C, knots, lev2knots, arg4 = None, weights_coeff = None, verbose = True):
    """produces a sparse grid starting from a multiindex-set rather than from 
    a rule IDXSET(I) <= W.
    
    [S,C] = smolyak_grid_multiidx_set(C,KNOTS,LEV2KNOTS) uses the multiindex 
    set C. C must be in lexicographic order and admissible.
    
    [S,C] = smolyak_grid_multiidx_set(C,KNOTS,LEV2KNOTS,S2), where S2 is 
    another Smolyak grid, tries to recycle tensor grids from S2 to build those 
    of S instead of recomputing them. This can be helpful whenever sequences 
    of Smolyak grid are generates. Note that *NO* check will performed whether 
    S2 was generated with the same lev2knots as the one given as input. S2 can 
    also be empty, S2=[].
    
    [S,C] = smolyak_grid_multiidx_set(C,KNOTS,LEV2KNOTS,MAP,WEIGHTS_COEFF) can 
    be used as an alternative to generate a sparse grid on a hyper-rectangle.
    
    See also CHECK_SET_ADMISSIBILITY for admissibility check, and SMOLYAK_GRID 
    for further information on KNOTS, LEV2KNOTS, MAP, WEIGHTS_COEFF and on the 
    sparse grid data structure S.
    """        
    if arg4 is not None:
        if callable(arg4):
            mapping = arg4
        elif issmolyak(arg4):
            S2 = arg4
        else:
            raise ValueError('SparseGKit:WrongInput -- unknown type for 6th input')
            
    # sort C in lexicographic order
    C, ordering = mysortrows(C)
    N = np.size(C,1)
    
    # if knots and  lev2knots are simple function, we replicate them in a cell
    if ins.isfunction(knots):
        fknots = knots
        knots = list(fknots for i in range(N))

    if ins.isfunction(lev2knots):
        f_lev2knots=lev2knots
        lev2knots = list(f_lev2knots for i in range(N))
        
    try:
        S2
        # get index set of S2. Note that if S2 has empty fields, C2==[]
        C2 = [sg.idx for sg in S2]
    except:
        pass
    
    # -----------------------------------------------

    # Each multiindex of C is a delta grid. Given say [i1 i2 i3] i will get
    # (i1 - i1-1) x (i2 - i2-1) x (i3 - i3-1)
    # that is the grids associated to
    # [i1 i2 i3],[i1-1 i2 13],[i1 i2-1 i3],[i1-1 i2-1 i3] and so on.
    #
    # Note that all of these multiindeces are also included in the initial set 
    # C (if [i1 i2 i3] ratisfies the rule, all other will, because their 
    # indeces are equal or lower).
    #
    # So the set of all possible grids (non delta grids) is the same set C, 
    # but some of them will cancel out.
    #
    # C is partially ordered (lexicographic): [x x x x i], are listed 
    # increasing with i, [x x x j i] are listed increasing first with j and 
    # then with i ...
    # Now take a row of C, c. Because of the ordering, if you take c as a grid 
    # index, the same grid can appear again only from delta grids coming from 
    # rows following.
    #
    # Now we scroll these rows following (say c2) and compute how many times c 
    # will be generated, and with what sign. It has to be that d=c2-c has only 
    # 0 or 1.
    #
    # If this is the case, the sign of this new appearence of c could be both 
    # + or - .
    # To determine the sign, start with + and switch it every time a 1 appears 
    # in d
    #
    # d=[0 1 0 0 1] => sign= +
    #
    # the formula is (-1)^sum(d) ( d with even appearences of 1 gives a + , odd
    # gives a -)
    #
    # and just sum all the coefficients to see if c will survive or cancel out
    #
    # so the algorithm works like this:
        
    nn = np.size(C, 0)
    coeff = np.ones(nn)     # initialize coefficients to 1: all c survive
    
    # I can at least restrict the search to multiindices whose first component 
    # is c(i) + 2, so I define
    _, bookmarks = np.unique(C[:, 0], return_index=True)
    bk = np.concatenate((bookmarks[2:]-1, [nn-1, nn-1]))
    # i.e. those who begin with 1 end at bookmark(3)-1, those who begin
    # with 2-1 end at bookmark(4) and so on, until there's no multiindex
    # with c(i)+2
    
    for i in range(nn):
        cc = C[i, :]
        # recover the range in which we have to look. Observe that the
        # first column of C contains necessarily 1,2,3 ...  so we can use
        # them to access bk
        rng = bk[cc[0]-1]
        for j in range(i+1, rng+1):
            # scroll c2, the following rows
            dd = C[j, :]-cc
            if np.max(dd) <= 1 and np.min(dd) >= 0:
                coeff[i] += (-1)**(np.sum(dd))
                
    # now we can store only those grids who survived, i.e. coeff~=0
    # ------------------------------------------------------
                
    nnz_coeff = np.nonzero(coeff)
    nb_grids=np.size(nnz_coeff[0])
    
    S = [None]*nb_grids
    nnz_pos= nnz_coeff[0]
    
    try:
        S2
        if verbose:
            print('build smolyak grid with tensor grid recycling')
        for k in range(nb_grids):
            l = nnz_pos[k]
            i = C[l, :]
            # this exploits that C2 is lexicographic, so it's efficient 
            # (cost ~ log(nb_rows_C2))
            [found, pos] = find_lexicographic(i, C2, 'nocheck')
            if found:
                S[k] = copy.deepcopy(S2[pos])
                S[k].weights = S[k].weights / S2[pos].coeff
            else:
                m = apply_lev2knots(i,lev2knots,N)
                S[k] = tensor_grid(N,m,knots)
            S[k].weights *= coeff[l]
            S[k].coeff = coeff[l]
            S[k].idx = i
    except:
        for k in range(nb_grids):
            l = nnz_pos[k]
            i = C[l, :]
            m = apply_lev2knots(i,lev2knots,N)
            S[k] = tensor_grid(N,m,knots)
            S[k].weights *= coeff[l]
            S[k].coeff = coeff[l]
            S[k].idx = i
            
    # finally, shift the points according to map if needed
    try:
        mapping
        if mapping != []:
            for ss in range(nb_grids):
                S[ss].knots = mapping(S[ss].knots)
    except NameError:
        pass        
    
    # and possibly fix weights
    if weights_coeff != None:
        for ss in range(nb_grids):
            S[ss].weights = np.dot(np.reshape(S[ss].weights, 
                             (-1,np.size(weights_coeff, 0))), weights_coeff)
            
    return S, C

def apply_lev2knots(i, lev2knots, N):
    """returns a vector m s.t. m(n) = m(i(n)).
    N could be deduced by i but it's better passed as an input, to speed up 
    computation.
    """
    # init m to zero vector
    m = 0 * i
    
    # next, iterate on each direction 0,...,N-1.
    if type(lev2knots) is list:
        for n in range(N):
            m[n] = lev2knots[n](toarray(i[n]))
    else:
        for n in range(N):
            m[n] = lev2knots(toarray(i[n]))
    m = m.astype(int)
    return m

def toarray(x):
    """Return numpy array of list or scalar."""
    return np.array([x] if np.isscalar(x) else x)











