import numpy as np
import inspect as ins
#from ..data_structures import TensorGrid
from ..src.tensor_grid import tensor_grid
from ..src.multiidx_gen import multiidx_gen
from ..src.issmolyak import issmolyak
from ..src.find_lexicographic import find_lexicographic
import copy

def smolyak_grid(N, w, knots, lev2knots, idxset=lambda i: sum(np.array(i)-1), 
                 arg6=None, weights_coeff=None):
    """generates a Smolyak sparse grid (and corresponding quadrature weights)
    as a linear combination of full tensor grids, employing formula (2.9)
    of [Nobile-Tempone-Webster, SINUM 46/5, pages 2309-2345].

    [S,C] = smolyak_grid(N,W,KNOTS,LEV2KNOTS,IDXSET) creates a sparse grid in 
    N dimensions using the multiindex set defined by IDXSET(I) <= W,
    where  I is N-dimensional multiindex, W is an integer non-negative value 
    and IDXSET is a function. IDXSET is an optional argument, the default being
    IDXSET = lambda i: sum(i-1).

    KNOTS is either a list containing the functions to be used to generate the 
    knots in each direction, i.e. 

    KNOTS={@knots_function1, @knots_function2, ... }

    or a single function to be used in every direction, i.e. 
    KNOTS=@knots_function1. In both cases, the header of knots_function is 
    x,w = knots_function(m).

    LEV2KNOTS is either a list containing the functions defining the relation 
    between level and number of knots to be used in each direction, i.e. 
    
    LEV2KNOTS={@m_function1, @m_function2, ... }

    or a single function to be used in every direction, i.e. 
    LEV2KNOTS=@m_function1. In both cases, the header of m_function is 
    m = m_function(i).

    The sparse grid information is stored as a list of "tensor grids", 
    each "tensor grid" S(j) is a four fields structure:
        S[j].knots: vector containing the tensor grid knots
        S[j].weights: vector containing the corresponding weights
        S[j].size: size of the tensor grid (= prod(m))
        S[j].knots_per_dim: list (N components), each component is the set of 
            1D knots used to build the tensor grid
        S[j].m: the input vector m, m == lev2knots(idx), 
            m(i)==length(S(j).knots_per_dim(i))
        S[j].coeff: how many times the tensor grid appears in the sparse grid 
            (with sign) the index j runs over all points in the level set Y(W,N) 
        S[j].idx: the multiidx vector corresponding to the current grid, whose 
            number of points is defined by lev2knots(i)

    The outputs of SMOLYAK_GRID are 
    S: structure containing the information on the sparse grid (vector of 
        tensor grids; see above)
    C: multi-index set used to generate the sparse grid  

    [S,C] = smolyak_grid(N,W,KNOTS,LEV2KNOTS,IDXSET,MAP,WEIGHTS_COEFF) can be 
    used as an alternative to generate a sparse grid on a hyper-rectangle. 
    Instead of typing out one KNOTS function and one LEV2KNOTS for each 
    dimension, like in
    
    [S,C] = smolyak_grid(N,W,{@knots1, @knots2, ...},{@m1, @m2 ...}),

    one can use 

    [S,C] = smolyak_grid(N,W,@KNOTS,@M,MAP,WEIGHTS_COEFF),

    which generates the sparse grid on the hypercube corresponding to @KNOTS 
    and and then shifts it according to the mapping defined by MAP, e.g. from 
    (-1,1)^N to (a1,b1)x(a2,b2)x...x(a_N,b_N). See also GET_INTERVAL_MAP. 
    Specifying the scalar value WEIGHTS_COEFF will also multiply the quadrature
    weights by WEIGHTS_COEFF. Use IDXSET=[] to use the default value, 
    IDXSET = @(i) sum(i-1).    
    """
    # distinguish between calls
    if arg6 != None:
        if callable(arg6):
            mapping = arg6
        elif issmolyak(arg6):
            S2 = arg6
        else:
            raise ValueError('SparseGKit:WrongInput -- unknown type for 6th input')
            
    # if knots and  lev2knots are simple function, we replicate them in a list
    if ins.isfunction(knots):
        fknots = knots
        knots = list(fknots for i in range(N))

    if ins.isfunction(lev2knots):
        f_lev2knots = lev2knots
        lev2knots = list(f_lev2knots for i in range(N))
        
    if idxset == [] or idxset == None:
        idxset = lambda i: sum(np.array(i)-1)

    if w == 0:
        # ----------------------------------------------------------
        # the trivial case
        i = np.ones(N)
        m = apply_lev2knots(i,lev2knots,N)
        S = [tensor_grid(N,m,knots)]
        S[0].coeff = 1
        S[0].idx = i
        C = i
    else:
        # ----------------------------------------------------------
        # let's go with the sparse construction
        
        # build the list of multiindices in the set: idxset(i)<=w.
        C = multiidx_gen(N,idxset,w,1)
        C = np.array(C)
        
        try:
            S2
            if S2 != []:
                nb_idx_C2 = len(S2)
                C2 = [S2[i].idx for i in range(nb_idx_C2)]
        except:
            pass
            
        # Now compute the tensor grids out of the delta operators listed in C.
        # Exploit partial ordering of the sequence of multiindices.
        # -----------------------------------------------
        #
        # Each multiindex of C is a delta grid. Given say [i1 i2 i3] I will get
        # (i1 - i1-1) x (i2 - i2-1) x (i3 - i3-1)
        # that is the grids associated to
        # [i1 i2 i3],[i1-1 i2 13],[i1 i2-1 i3],[i1-1 i2-1 i3] and so on.
        #
        # Note that all of these multiindices are also included in the initial 
        # set C (if [i1 i2 i3] satisfies the rule, all other will as well, 
        # because their indices are equal or lower).
        #
        # So the set of all possible grids (non delta grids) is the same set C,
        # but some of them will cancel out.
        #
        # C is partially ordered (lexicographically): [x x x x i], are listed 
        # increasing with i, [x x x j i] are listed increasing first with j and
        # then with i ...
        # Now take a row of C, c. Because of the ordering, if you take c as a 
        # grid index the same grid can appear again only from delta grids 
        # coming from rows following.
        #
        # Now we scroll these rows following (say c2) and compute how many 
        # times c will be generated, and with what sign. It has to be that 
        # d=c2-c has only 0 or 1.
        #
        # if this is the case, the sign of this new appearence of c could be 
        # both + or - .
        # To determine the sign, start with + and switch it every time a 1 
        # appears in d
        #
        # d=[0 1 0 0 1] => sign= +
        #
        # the formula is (-1)^sum(d) ( d with even appearences of 1 gives a + ,
        # odd gives a -) and just sum all the coefficients to see if c will 
        # survive or cancel out
    
        nn = C.shape[0]
        coeff = np.ones(nn)     # initialize coefficients to 1: all c survive
        
        # I can at least restrict the search to multiindices whose first 
        # component is c(i) + 2, so I define
        _, bookmarks = np.unique(C[:, 0], return_index=True)
        bk = np.concatenate((bookmarks[2:]-1, [nn-1, nn-1]))
        # i.e. those who begin with 1 end at bookmark(3)-1, those who begin
        # with 2-1 end at bookmark(4) and so on, until there's no multiindex
        # with c(i)+2
        for i in range(nn):
            cc = C[i, :]
            # recover the range in which we have to look. Observe that the
            # first column of C contains necessarily 1,2,3 ...  so we can use
            # them to access bk
            rng = bk[cc[0]-1]
            for j in range(i+1, rng+1):
                # scroll c2, the following rows
                dd = C[j, :]-cc
                if np.max(dd) <= 1 and np.min(dd) >= 0:
                    coeff[i] += (-1)**(np.sum(dd))
                    
        # now we can store only those grids who survived, i.e. coeff != 0
        # ------------------------------------------------------
        nnz_coeff = np.nonzero(coeff)
        nb_grids = np.size(nnz_coeff[0])

        S = [None]*nb_grids
        nnz_pos = nnz_coeff[0]
        
        try:
            C2
            if C2 != []:
                for k in range(nb_grids):
                    l = nnz_pos[k]
                    i = C[l,:]
                    # this exploits that C2 is lexicographic, so it's efficient 
                    # (cost ~ log(nb_rows_C2))
                    found, pos = find_lexicographic(i, C2, 'nocheck')
                    if found:
                        S[k] = copy.deepcopy(S2[pos])
                        # however we need to fix the weights. Indeed, they are 
                        # stored in S2 as weights*coeff, so we need to reverse
                        # that multiplication
                        S[k].weights = S[k].weights/S2[pos].coeff
                    else:
                        m = apply_lev2knots(i, lev2knots, N)
                        S[k] = tensor_grid(N, m, knots)
                    S[k].weights *= coeff[l]
                    S[k].coeff = coeff[l]
                    S[k].idx = i
        except:
            for k in range(nb_grids):
                l = nnz_pos[k]
                i = C[l,:]
                # n. of points in each direction
                m = apply_lev2knots(i, lev2knots, N) 
                S[k] = tensor_grid(N,m,knots)
                S[k].weights *= coeff[l]
                S[k].coeff = coeff[l]
                S[k].idx = i
    
    # finally, shift the points according to map if needed
    try:
        mapping
        if mapping != []:
            for ss in range(nb_grids):
                S[ss].knots = mapping(S[ss].knots)
    except NameError:
        pass        
    
    # and possibly fix weights
    if weights_coeff != None:
        for ss in range(nb_grids):
            S[ss].weights = np.dot(np.reshape(S[ss].weights, 
                             (-1,np.size(weights_coeff, 0))), weights_coeff)

    return S, C

def apply_lev2knots(i, lev2knots, N):
    """returns a vector m s.t. m(n) = m(i(n)).
    N could be deduced by i but it's better passed as an input, to speed up 
    computation.
    """
    # init m to zero vector
    m = 0 * i
    
    # next, iterate on each direction 0,...,N-1.
    if type(lev2knots) is list:
        for n in range(N):
            m[n] = lev2knots[n](toarray(i[n]))
    else:
        for n in range(N):
            m[n] = lev2knots(toarray(i[n]))
    m = m.astype(int)
    return m

def toarray(x):
    """Return numpy array of list or scalar."""
    return np.array([x] if np.isscalar(x) else x)












