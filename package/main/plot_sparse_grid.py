import matplotlib.pyplot as plt
from ..src.issmolyak import issmolyak
from ..src.istensor import istensor
from ..src.isreduced import isreduced
from .reduce_sparse_grid import reduce_sparse_grid

def plot_sparse_grid(S, dims = [0,1], *argv):
    """PLOT_SPARSE_GRID(S) plots S, which is a sparse grid in 2D. S can be 
    either reduced or not. S can also be a tensor grid (use istensor(S) to 
    verify).
    
    PLOT_SPARSE_GRID(S,[d1 d2]) plots the d1 and d2 components of the points 
    in S if S is more than 2D.
    """
    if istensor(S):
        pts = S.knots
    elif issmolyak(S):
        Sr = reduce_sparse_grid(S)
        pts = Sr.knots
    elif isreduced(S):
        pts = S.knots
    else:
        raise ValueError('Grid not recognized')
    #print(argv{:})
    if len(argv)>0:
        plt.plot(pts[dims[0],:], pts[dims[1],:], argv[0])
    else:
        plt.plot(pts[dims[0],:], pts[dims[1],:], 'o')