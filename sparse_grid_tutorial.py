import time
import numpy as np
import matplotlib.pyplot as plt
import copy
import package.tools.knots_functions as knots_fun
import package.main.smolyak_grid as sm_gr
import package.tools.lev2knots_functions as lev2knots_fun
import package.main.plot_sparse_grid as plot
import package.src.tensor_grid as tengr
import package.tools.idxset_functions as idxset_fun
import package.src.issmolyak as issmolyak
import package.src.istensor as istensor
import package.main.smolyak_grid_multiidx_set as sm_gr_multiidx_set
import package.src.multiidx_gen as mult_gen
import package.src.mysortrows as mysortrows
import package.tools.rescaling_functions as resc_fun
import package.main.reduce_sparse_grid as red_sp
import package.src.compare_sparse_grids as com_sp_gr



# PART 1: INTRODUCTION - WHAT IS A SPARSE GRID

# A sparse grid is a linear combination of many tensor grids on R^N 
# (parameter space). Each of the tensor grids included has ``few points''. 
# With suitable linear combinations of such grids, it is possible to achieve 
# good accuracy in quadrature and interpolation, with a computational cost 
# lower than using a single tensor grid.

# Run these commands to build a sparse grid and visualize each component.

N = 2 # approximation of two variables
knots = lambda n: knots_fun.knots_CC(n, -1, 1, 'nonprob')
w = 3 # level
S = sm_gr.smolyak_grid(N, w, knots, lev2knots_fun.lev2knots_doubling)[0] # grid

# visualization
# plot the grid itself
plot.plot_sparse_grid(S)

# each component
plt.figure()
s_max = len(S)
k = 0
for s in range(s_max):
    if S[s].size is not None:
        k += 1
        plt.subplot(2,4,k)
        plot.plot_sparse_grid(S[s])
        plt.axis('equal')
        
# =============================================================================        
# PART 1: INTRODUCTION - INGREDIENTS OF A SPARSE GRID. 1D KNOTS 
# Each of the tensor grids in the sparse grid is built by taking cartesian 
# products of 1D distribution of points (in general a different number of 
# points in each direction). The Sparse Grid Matlab Kit provides several knots 
# families. These functions also return the quadrature weights associated to 
# the knots (more on this later).

# Gauss-Legendre points: quadrature points to approximate integrals like 
# \int_a^b f(x) dx with n points
n = 5; a = 1; b = 4
x = knots_fun.knots_uniform(n, a, b)[0]

plt.figure()
plt.plot(x, 0*x, 'ok', markerfacecolor='k', label='5 GL points')
plt.grid()

# Clenshaw-Curtis points: nested quadrature points to approximate integrals 
# like \int_a^b f(x) dx with n points. If one "doubles" the number of points, 
# the new points will include the old ones.

x = knots_fun.knots_CC(n, a, b)[0]
plt.plot(x, 1 + 0*x, 'or', MarkerFaceColor='r', label='5 CC points')

n = 9
x = knots_fun.knots_uniform(n, a, b)[0]
plt.plot(x, -1 + 0*x, 'ob', MarkerFaceColor='b', 
         label='9 GL points (does NOT include the 5 points)')

x = knots_fun.knots_CC(n, a, b)[0]
plt.plot(x, 2 + 0*x, 'og', MarkerFaceColor='g', 
         label='9 CC points (includes the 5 points)')

plt.ylim([-1.5, 4])
plt.legend()

# Leja points: nested quadrature points to approximate integrals like 
# \int_a^b f(x) dx with n points. Three different kind of Leja points are 
# available: Line Leja, sym-Line Leja, p-disk Leja (see leja_points.m for more 
# details). All Leja points are nested by construction.

plt.figure()

# ------- line leja --------
n = 5; a = 1; b = 4
x = knots_fun.knots_leja(n, a, b, 'line')[0]
plt.plot(x, 1 + 0*x, 'or', markerfacecolor = 'r', label = '5 line Leja points')

n = 9
x = knots_fun.knots_leja(n, a, b, 'line')[0]
plt.plot(x, 2 + 0*x, 'or', markerfacecolor = 'r', label = '9 line Leja points')

# ------- sym leja ----------
n = 5
x = knots_fun.knots_leja(n, a, b, 'sym_line')[0]
plt.plot(x, 3 + 0*x, 'ok', markerfacecolor = 'k', 
         label = '5 sym-line Leja points')

n = 9
x = knots_fun.knots_leja(n, a, b, 'sym_line')[0]
plt.plot(x, 4 + 0*x, 'ok', markerfacecolor = 'k', 
         label = '9 sym-line Leja points')

# ------- p-disk leja ----------
n = 5
x = knots_fun.knots_leja(n, a, b, 'p_disk')[0]
plt.plot(x, 5 + 0*x, 'ob', markerfacecolor = 'b', 
         label = '5 p-Disk Leja points')

n = 9
x = knots_fun.knots_leja(n, a, b, 'p_disk')[0]
plt.plot(x, 6 + 0*x, 'ob', markerfacecolor = 'b', 
         label = '9 p-Disk Leja points')

plt.legend()
plt.grid()
plt.ylim([-1.5, 12])
plt.show()

# =============================================================================
# Gauss-Hermite points: quadrature points to approximate integrals like
#
# 1/sqrt(2 sig pi) \int_R f(x) e^{ -(x-mi)^2 / (2 sig^2) } dx 
#
# with n points

plt.figure()

n = 9; mu = 0; sig = 1
x = knots_fun.knots_gaussian(n, mu, sig)[0]
plt.plot(x, 0*x, 'ok', markerfacecolor='k', label='9 GH points')

# Kronrod - Patterson Nodes : nested quadrature points to approximate integrals
#  as the previous

n = 3
x = knots_fun.knots_kpn(n)[0]
plt.plot(x, 1 + 0*x, 'or', markerfacecolor='r', label='3 KPN points')

n = 9
x = knots_fun.knots_kpn(n)[0]
plt.plot(x, 2 + 0*x, 'ob', markerfacecolor='b', label='9 KPN points')

# Gaussian-Leja : nested quadrature points to approximate integrals as the 
# previous

n = 3
x = knots_fun.knots_gaussian_leja(n)[0]
plt.plot(x, 3 + 0*x,'xr', LineWidth=2, MarkerFaceColor='r', MarkerSize=8,
         label ='3 Gaussian-Leja points')

n = 9
x = knots_fun.knots_gaussian_leja(n)[0]
plt.plot(x, 4 + 0*x,'xb', LineWidth=2, MarkerFaceColor='b', MarkerSize=8,
         label ='9 Gaussian-Leja points')

plt.legend()
plt.ylim([-1.5, 8])
plt.legend()
plt.show()

# ============================================================================= 
# PART 1: INTRODUCTION - INGREDIENTS OF A SPARSE GRID. LEV2KNOTS FUNCTION.
#
# In view of building sparse grids, it is useful to order 
# quadrature/interpolation rules in sequences, i.e. to introduce levels for the
#  rules. The Sparse Grid Matlab Kit provides 3 functions to this end:
#
# -> lev2knots_lin     
#
# adds 1 point from one level to the next: consecutive quadrature/interpolation
# rules have 1,2,3,4,..points
print(lev2knots_fun.lev2knots_lin([1,2,3,4,5]))

# -> lev2knots_2step     
#
# adds 2 points from one level to the next: consecutive quadrature/interpolation 
# rules have 1,3,5,7,..points
print(lev2knots_fun.lev2knots_2step([1,2,3,4,5]))

# -> lev2knots_doubling 
#
# "doubles" the number of points from one level to the next: consecutive rules 
# have 1,3,5,9,17... points
print(lev2knots_fun.lev2knots_doubling([1,2,3,4,5]))

# -> lev2knots_kpn
#
# needed when using kpn knots which are tabulated. consecutive rules have 
# 1,3,9,19,35 points. The latter is the finest resolution possible.
print(lev2knots_fun.lev2knots_kpn([1,2,3,4,5]))

# ============================================================================= 
# PART 1: INTRODUCTION - INGREDIENTS OF A SPARSE GRID. MULTI-INDEX SET
#
# The last ingredient to specify when building a sparse grid is the set of 
# tensor grids to be used. The algorithm will then take care of computing the 
# coefficients of the linear combination of these grids (note that such 
# coefficients may be 0 as well). 

# The most convenient way to specify tensor grids is to use multi-index 
# notation: every grid is associated to a multiindex, that is a vector of 
# integer numbers. Each number in the vector tells the level of the quadrature 
# rule used in each direction of the parameter space. E.g. : the multiindex 
# [3 5] is associated to the tensor grid using a quad rule of level 3 in 
# direction 1, and level 5 in direction 2. The actual number of points in each 
# direction depends by the level-knots relation specified by the 
# lev2knots_*** function.

N = 2
ii = [3, 5]
knots = lambda n: knots_fun.knots_uniform(n, -1, 1, 'nonprob')

S_lin = tengr.tensor_grid(N, lev2knots_fun.lev2knots_lin(ii), knots)
S_doub = tengr.tensor_grid(N, lev2knots_fun.lev2knots_doubling(ii), knots)

plt.figure()
plot.plot_sparse_grid(S_doub, [0,1], 'o')
plot.plot_sparse_grid(S_lin)
plt.show()

# There are two ways of specifying the set of multindices to be used. 
#
# 1) The first one is to use the parameters "level" and "idxset" of the 
# function SMOLYAK. 
# In this case, the multiindex set will include all the multiindices that 
#satisfy the inequality
#
# idxset(ii)<= level
#
# by default, idxset is set to @(ii) sum(ii-1). The combination of idxset 
# function and lev2knots function defines the sparse grid type: 
# using @(ii) sum(ii-1) with lev2knots_lin results in the so-called 
# TD (Total Degree) tensor grid, while  
# @(ii) sum(ii-1) with lev2knots_doubling in the original SM (Smolyak) grid.
# Some choices are available by using the function 
#
#  lev2knots, idxset = idxset_fun.define_functions_for_rule(rule,rates)
#
# but any other set satisfying the so-called ``admissibility condition'' 
# (see e.g. Gerstner-Griebel ``Dimension-Adaptive Tensor-Product Quadrature''). 
# can be used.

N = 2
knots = lambda n: knots_fun.knots_uniform(n, -1, 1, 'nonprob')
w = 5   # level

lev2knots, idxset = idxset_fun.define_functions_for_rule('TD', N)
S_TD = sm_gr.smolyak_grid(N, w, knots, lev2knots, idxset)[0]   # grid

lev2knots, idxset = idxset_fun.define_functions_for_rule('HC', N)
S_HC = sm_gr.smolyak_grid(N, w, knots, lev2knots, idxset)[0]   # grid

plt.figure()
plot.plot_sparse_grid(S_TD)
plot.plot_sparse_grid(S_HC)
plt.show()

# 2) The second one is to use the function SMOLYAK_MULTIINDICES, where one 
# specifies exactly the set of multiindex that one wishes to use. Again, the 
# set has to satisfy the ``admissibility condition'', and the rows have to be 
# in lexicographic order. 

C = [[1, 1], [1, 2], [1, 3], [1, 4], [2, 1], [2, 2]]

adm, C = idxset_fun.check_set_admissibality(C)

S_M = sm_gr_multiidx_set.smolyak_grid_multiidx_set(C, knots, lev2knots)[0]

plt.figure()
plot.plot_sparse_grid(S_M)
plt.show()

# =============================================================================
# The package provides two functions to generate multi-index sets.

# a) MULTIIDX_BOX_SET generates all multiindices jj that are component-wise 
# less than or equal to some other index ii. The minimal value of the 
# components of the indices to be generated can be either 0 or 1. For instance

jj = [2, 3]
C = np.array(idxset_fun.multiidx_box_set(jj, 0)[0])
D = np.array(idxset_fun.multiidx_box_set(jj, 1)[0])

plt.figure()
plt.plot(C[:,0], C[:,1], 'xr', markerfacecolor='r', linewidth=2, markersize=12,
         label = 'Multiidx box set, min=0')
plt.plot(D[:,0], D[:,1], 'ok', markerfacecolor='k', 
         label = 'Multiidx box set, min=1')
plt.legend()
plt.xlim([-0.5, 4])
plt.ylim([-0.5, 4])
plt.show()

# b) MULTIIDX_BOX_GEN generates the set of all indices ii such that rule(ii)<=w,
# where rule is a function that takes as input a row vector (or a matrix where 
# each multiidx is stored as a row) and returns a scalar value (or a column 
# vector with the result of the operation applied to each row of the input 
# index vector). Again, the minimum index can be 0 or 1:

N = 2
w = 7
rule = lambda I: np.sum(np.reshape(np.array(I), (1,-1)), axis=1)
E = np.array(mult_gen.multiidx_gen(N, rule, w, 0))
F = np.array(mult_gen.multiidx_gen(N, rule, w, 1))

plt.figure()
plt.plot(E[:,0], E[:,1], 'xr', markerfacecolor='r', linewidth=2, markersize=12,
         label='Multiidx gen, min=0')
plt.plot(F[:,0], F[:,1], 'ok', markerfacecolor='k', label='Multiidx gen, min=1')
plt.legend()
plt.xlim([-0.5,8])
plt.ylim([-0.5,8])
plt.show()

# =============================================================================
# when building a large sparse grid, it might be useful to recycle from 
# previous grids to speed-up the computation

knots = lambda n: knots_fun.knots_gaussian(n, 0, 1)
lev2knots = lev2knots_fun.lev2knots_lin

N = 20
w = 4
S = sm_gr.smolyak_grid(N, w, knots, lev2knots, lambda i: np.prod(np.array(i)))[0]
w = 5

print('build grid without recycling')
start = time.time()
T = sm_gr.smolyak_grid(N, w, knots, lev2knots, lambda i: np.prod(np.array(i)))[0]
stop = time.time()
print("Elapsed time: ", stop - start)

print('build grid with recycling')
start = time.time()
T_rec = sm_gr.smolyak_grid(N, w, knots, lev2knots,
                           lambda i: np.prod(np.array(i)), S)[0]
stop = time.time()
print("Elapsed time: ", stop - start)
# =============================================================================
# note that the following call is also valid: 
#T_rec = sm_gr.smolyak_grid(N, w, knots, lev2knots,
#                           lambda i: np.prod(np.array(i)), [])[0]

start = time.time()
for w in range(1,7):
    # build grid
    T = sm_gr.smolyak_grid(N, w, knots, lev2knots, 
                           lambda i: np.prod(np.array(i)))[0]
stop = time.time()
print("Elapsed time: ", stop - start)

start = time.time()
T_old = []
for w in range(1,7):
    # build grid
    T_rec = sm_gr.smolyak_grid(N, w, knots, lev2knots, 
                           lambda i: np.prod(np.array(i)), T_old)[0]
    T_old = copy.deepcopy(T)
stop = time.time()
print("Elapsed time: ", stop - start)

# =============================================================================
# the same functionality is also available for smolyak_grid_multiidx_set
knots = lambda n: knots_fun.knots_gaussian(n, 0, 1)
lev2knots = lev2knots_fun.lev2knots_lin
ibox = [3,4,2,4,2]
_, C = idxset_fun.multiidx_box_set(ibox, 1)
D = mysortrows.mysortrows(C + [[2,5,2,2,6]])[0]

print("build smolyak grid without tensor grid recycling")
S = sm_gr_multiidx_set.smolyak_grid_multiidx_set(C, knots, lev2knots)[0]
start = time.time()
T = sm_gr_multiidx_set.smolyak_grid_multiidx_set(D, knots, lev2knots)[0]
stop = time.time()
print("Elapsed time: ", stop - start)

start = time.time()
T_rec = sm_gr_multiidx_set.smolyak_grid_multiidx_set(D, knots, lev2knots, S)[0]
stop = time.time()
print("Elapsed time: ", stop - start)

# =============================================================================
# A sparse grid is represented as a vector of structures. Each element is a 
# tensor grid, with fields containing the knots, the corresponding integration 
# weights, its coefficient in the linear combination, and the number of points.

# In general, the following conventions hold:
# -> points in the space of parameters are columns-vector
# -> multiindices are row-vector

# =============================================================================
# It is easy to modify the domain of a sparse grid from (-1,1)^N to other 
# hyper-rectangles. Two options are available
N = 2

# 1) Generate knots on the desired hyper-rectangle (here (0,2)^2 ).
knots = lambda n: knots_fun.knots_CC(n, 0, 2, 'nonprob')
w = 4
S = sm_gr.smolyak_grid(N, w, knots, lev2knots_fun.lev2knots_doubling)[0]

# 2) Alternatively, use the standard interval and provide a shifting function 
# to smolyak_grid. 
mapping = resc_fun.get_interval_map([0, 0],[2, 2],'uniform')
knots = lambda n: knots_fun.knots_CC(n, -1, 1, 'nonprob')
S2 = sm_gr.smolyak_grid(N, w, knots, lev2knots_fun.lev2knots_doubling, [], mapping)[0]


print("maximum difference between corresponding points in the two grids")
print(max([np.max(np.abs(a.knots - b.knots)) for a,b in zip(S,S2)]))

plt.figure()
plot.plot_sparse_grid(S)
plot.plot_sparse_grid(S2)
plt.show()

# One can mix different intervals / different knots families on different 
# directions. 
N = 2

knots1 = lambda n: knots_fun.knots_CC(n, 0, 2, 'nonprob')
knots2 = lambda n: knots_fun.knots_uniform(n, -1, 5, 'nonprob')
w = 4
S = sm_gr.smolyak_grid(N, w, [knots1, knots2], [lev2knots_fun.lev2knots_doubling,
                       lev2knots_fun.lev2knots_lin])[0]
plt.figure()
plot.plot_sparse_grid(S)

# In case knots and lev2knots functions in the different directions are the 
# same and the only thing that changes is the definition interval, also using 
# the standard interval and providing a shifting function to smolyak_grid will do.
N = 2

knots1 = lambda n: knots_fun.knots_CC(n, 0, 2, 'nonprob')
knots2 = lambda n: knots_fun.knots_CC(n, -1, 5, 'nonprob')
w = 4
S = sm_gr.smolyak_grid(N, w, [knots1, knots2], 
                       lev2knots_fun.lev2knots_doubling)[0]

mapping = resc_fun.get_interval_map([0, -1], [2, 5], 'uniform')
knots = lambda n: knots_fun.knots_CC(n, -1, 1, 'nonprob')
S2 = sm_gr.smolyak_grid(N, w, knots, lev2knots_fun.lev2knots_doubling, [], 
                        mapping)[0]

print("maximum difference between corresponding points in the two grids")
print(max([np.max(np.abs(a.knots - b.knots)) for a,b in zip(S,S2)]))

plt.figure()
plot.plot_sparse_grid(S)
plot.plot_sparse_grid(S2)
plt.show()

# ============================================================================= 
# PART 1: INTRODUCTION - REDUCE A SPARSE GRID

# The tensor grids forming the sparse grid may have points in common (even 
# when using non-nested points). To save computational time during e.g. 
# evaluation of a function on a sparse grid, it is then important to get rid 
# of these repetions. To this end, use the function reduce_sparse_grid. The 
# quadrature weights are of course consistently modified. The field "size" 
# tells the number in the reduced grid.
N = 2
w = 5
knots = lambda n: knots_fun.knots_CC(n, -1, 1, 'nonprob')

lev2nodes, idxset = idxset_fun.define_functions_for_rule('SM', N)
S = sm_gr.smolyak_grid(N, w, knots, lev2nodes, idxset)[0]
Sr = red_sp.reduce_sparse_grid(S)

print('size of original grid: ', sum([np.size(a.knots,1) for a in S]))
print('size of reduced grid: ', np.size(Sr.knots, 1))
print('Sr.size: ', Sr.size)

plt.figure()
plt.subplot(1,2,1)
plot.plot_sparse_grid(S)
plt.axis('equal')

plt.subplot(1,2,2)
plot.plot_sparse_grid(Sr)
plt.axis('equal')
plt.show()

# ============================================================================= 
# PART 2: EVALUATE A FUNCTION ON A SPARSE GRID - BASICS

# The kit comes with the function evaluate_on_sparse_grid, that allows to  
# evaluate a function on the points of a sparse grid, and provides
# -> recycling of previous evaluations 
# -> support for parallel evaluations. 
# Works for scalar-valued as well as vector-valued functions. Sparse grids 
# passed as input must be reduced.

fs = lambda x: sum(x)
fv = lambda x: 2*x

N = 2; w = 3
#S = sm_gr(N, w, lambda n: knots_fun.knots_uniform(n, -1, 1),lev2knots_fun.lev2knots_lin)[0]
#Sr = red_sp.reduce_sparse_grid(S)

# plain use of evaluate_on_sparse_grid: no recycling
knots = lambda n: knots_fun.knots_CC(n, -1, 1, 'nonprob')
lev2nodes, idxset = idxset_fun.define_functions_for_rule('SM', N)


C = [[1, 1], [1, 2], [1, 3], [1, 4], [2, 1], [2, 2], [3, 1]]
S, C = sm_gr_multiidx_set.smolyak_grid_multiidx_set(C, knots, lev2nodes)
Sr = red_sp.reduce_sparse_grid(S)

C2 = [[1, 1], [1, 2], [2, 1], [2, 2], [3, 1], [3, 2], [4, 1]]
S2, C2 = sm_gr_multiidx_set.smolyak_grid_multiidx_set(C2, knots, lev2nodes)
Sr2 = red_sp.reduce_sparse_grid(S2)

com_sp_gr.compare_sparse_grids(S, Sr, S2, Sr2)


#C = []
#
#N = 2
#w = 3
#knots = lambda n: knots_fun.knots_CC(n, -1, 1, 'nonprob')
#
#lev2nodes, idxset = idxset_fun.define_functions_for_rule('SM', N)
#S, C = sm_gr.smolyak_grid(N, w, knots, lev2nodes, idxset)
#Sr = red_sp.reduce_sparse_grid(S)
#
#w = 3
#S2, C2 = sm_gr.smolyak_grid(N, w, knots, lev2nodes, idxset)
#Sr2 = red_sp.reduce_sparse_grid(S2)
#
#com_sp_gr.compare_sparse_grids(S, Sr, S2, Sr2)












































        
        

