Sparse grid collocation method is a technique which provides an approximation of a random quantity or solution to a random equation, estimation of statistical moments to the solution or statistics for a QoI of the solution.

Run the project as

python3 sparse_grid_tutorial.py

where you can see usage of particular features of the library.

For more information contact me on eva.vidlickova@gmail.com.